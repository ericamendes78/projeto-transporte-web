const apiUrl = 'http://localhost:8080/';
const button = document.querySelector('#btn-pesquisar');
const buttonPrevisao = document.querySelector('#btn-previsao');
const descricaoLinha = document.querySelector('#linhaOnibus');
const div = document.querySelector('#aviso');
const linha = document.querySelector('#inputLinha');
const parada = document.querySelector('#inputPontoDePartida');
let respostaApi = document.querySelector('#outputPrevisao');
let codigoParada;



function get(endpoint){
    return fetch(apiUrl + endpoint, {
        method: 'get',
        headers: {
            'content-type': 'application/json'
        }
    }).then((response) => {
        return response.json();
    });
}

function post(endpoint, data){
    return fetch(apiUrl + endpoint,
    {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'content-type': 'application/json'
        }
    }).then((response) => {
        return response.json();
    });
}

//////////// AQUI
function parseDataPrevisao(dados){

    let i = 0;

    let horario = dados.value.hr;
    respostaApi.innerHTML =  horario;

}

function parseDataParada(dados){
    codigoParada = dados.value.cp;
    respostaApi.innerHTML =  codigoParada;
}

///////////// AQUI

function parseDataLinha(dados){

    let i = 0;
    //<img id="onibus" name="onibus"  alt="Onibus" src="imagens/Bus-2-512.png"/>

    for(i = 0; i < dados.length; i++){
        let linha = dados[i];

        let divLinhas = document.createElement("div");
        divLinhas.id = linha.lt +''+ linha.sl;
        divLinhas.alt = linha.lt +''+ linha.sl;
        divLinhas.style.width = "100vw";
        divLinhas.style.height = "80px";    
        divLinhas.style.textAlign = "center";
        divLinhas.style.textJustify = "center";
        divLinhas.style.textTransform = "center";

        if(linha.sl == 1){
            divLinhas.innerHTML = `${linha.lt}-${linha.tl} <p> ${linha.tp} - ${linha.ts} `;
        }else{
            divLinhas.innerHTML = `${linha.lt}-${linha.tl} <p> ${linha.ts} - ${linha.tp} `;
        }

        
        div.appendChild(divLinhas);
        div.appendChild(button);
        // divLinhas.onclick = mostrarHorarioParada(linha.cl);
    }

}

function buscarLinha(){
    div.innerHTML = "";

    if(descricaoLinha.value != ''){
        get(`onibus/linha/${descricaoLinha.value}`).then(parseDataLinha);
    }else{
        alert('Informe o numero ou nome da linha para pesquisa.');
    }
}

function buscarResultadoPrevisao(){
  
    buscarCodigoParada();
    if(linha.value != '' & codigoParada.value != ''){
        get(`onibus/previsao/${parada.value}/${linha.value}`).then(parseDataPrevisao);
    }else{
        alert('Informe corretamente os itens de pesquisa.');
    }
}

function buscarCodigoParada(){
  
    if(parada.value != ''){
        get(`onibus/parada/${parada.value}`).then(parseDataParada);
    }else{
        alert('Informe corretamente os itens de pesquisa.');
    }
}




// APIS que vamos usar 

function buscarPrevisao(){
    get(`onibus/previsao/${descricaoParada.value}/${descricaoLinha.value}`).then(parseDataPrevisao);
}
function buscarParada(){
    get(`onibus/parada/${descricaoLinha.value}`).then(parseDataParada);
}

function buscarParadaPorLinha(){
    get(`onibus/parada/linha/${descricaoLinha.value}`).then(parseDataParadaPorLinha);
}
buttonPrevisao.onclick = buscarResultadoPrevisao;
button.onclick = buscarLinha;



// APIS que NAO vamos usar 
/*
function mostrarHorarioParada(codigoLinha){
     get(`onibus/linha/${descricaoLinha.value}`).then(parseDataLinha);
}

function buscarLinhaSentido(){
    get(`onibus/linha/${descricaoLinha.value}/${sentido.value}`).then(parseDataLinhaSentido);
}


function buscarParadaPorCorredor(){
    get(`onibus/parada/corredor/${descricaoCorredor.value}`).then(parseDataParadaPorCorredor);
}

function buscarCorredor(){
    get('onibus/parada/corredor').then(parseDataCorredor);
}

function buscarPosicao(){
    get('onibus/parada/posicao').then(parseDataPosicao);
}

function buscarPosicaoLinha(){
    get(`onibus/parada/${descricaoLinha.value}`).then(parseDataPosicaoLinha);
}
function buscarPrevisaoLinha(){
    get(`onibus/previsao/linha/${descricaoLinha.value}`).then(parseDataPrevisaoLinha);
}

function buscarPrevisaoParada(){
    get(`onibus/previsao/parada/${descricaoParada.value}`).then(parseDataPrevisaoParada);
}

*/


