const apiUrl = 'http://localhost:8080/';
const buttonPrevisao = document.querySelector('#btn-previsao');
const descricaoLinha = document.querySelector('#linhaOnibus');
const linha = document.querySelector('#inputLinha');
const parada = document.querySelector('#inputPontoDePartida');
let respostaApi = document.querySelector('#outputPrevisao');
var codigoParada;



function get(endpoint){
    return fetch(apiUrl + endpoint, {
        method: 'get',
        headers: {
            'content-type': 'application/json'
        }
    }).then((response) => {
        return response.json();
    });
}

function post(endpoint, data){
    return fetch(apiUrl + endpoint,
    {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'content-type': 'application/json'
        }
    }).then((response) => {
        return response.json();
    });
}

 
function parseDataPrevisao(dados){
    let i = 0;

    for(i = 0; i < dados.ps.length; i++){

        let linha = dados.ps[i];

        for(i = 0; i < linha.vs.length; i++){

        let veiculo = linha.vs[i];
    
        alert(veiculo.t);
    
        }
        
    }

}

function buscarCodigoParada(){
    if(linha.value != ''){
        get(`onibus/parada/${linha.value}`).then(parseCodigoParada);
        //buscarResultadoPrevisao();
    }else{
        alert('Informe corretamente os itens de pesquisa.');
    }
}

function parseCodigoParada(dados){
    codigoParada = dados.hr.value;
    respostaApi.innerHTML = codigoParada;
}

function buscarResultadoPrevisao(){
        if(linha.value != '' && parada.value != ''){
        get(`onibus/previsao/${parada.value}/${linha.value}`).then(parseDataPrevisao);
    }else{
        alert('Informe corretamente os itens de pesquisa.');
    }
}
// APIS que vamos usar 

function buscarPrevisao(){
    get(`onibus/previsao/${descricaoParada.value}/${descricaoLinha.value}`).then(parseDataPrevisao);
}
function buscarParada(){
    get(`onibus/parada/${descricaoLinha.value}`).then(parseDataParada);
}

function buscarParadaPorLinha(){
    get(`onibus/parada/linha/${descricaoLinha.value}`).then(parseDataParadaPorLinha);
}

buttonPrevisao.onclick = buscarCodigoParada;
