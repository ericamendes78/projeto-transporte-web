const buttonLogin = document.querySelector('#btn-logar');
const buttonSenha = document.querySelector('#btn-senha');
const buttonCadastro = document.querySelector('#btn-cadastrar');
const dadosUsername = document.querySelector('#username');
const dadosSenha= document.querySelector('#senha')
const divs = document.querySelector('section div');
const funcaoBusca = document.querySelector('section div');

function postInit(endpoint, data){

    var deuCerto;

    fetch(apiUrl + endpoint,
    {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Authorization': localStorage.getItem('token'), 
            'content-type': 'application/json'
        }
    }).then((response) => {

        if(response.headers.get('Authorization')){
            localStorage.setItem('token', response.headers.get('Authorization'));
        }
        
        if(response.ok){
            get(`usuario/username/${dadosUsername.value}`).then((response) => {
               localStorage.setItem('nome', response.nome);
               window.location.href = "home.html"; 
            });
            
        }else{
            alert('Usuário inválido');
        }
 
    });

    
}


function realizarLogin(){

    let data = {
        username : document.querySelector("#username").value,
        senha : document.querySelector('#senha').value

    };

    postInit("login", data);
   
}

function recuperarSenha(){

    window.location.href = "errors/404.html";
}

function cadastrarUsuario(){

    window.location.href = "usuario.html";
}


buttonLogin.onclick = realizarLogin;
buttonSenha.onclick = recuperarSenha;
buttonCadastro.onclick = cadastrarUsuario;




