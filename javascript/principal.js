const apiUrl = "http://142.93.22.210/";
let nomeUsuario = document.querySelector('#nomeUsuario') || {};

nomeUsuario.innerHTML = localStorage.getItem('nome');

function get(endpoint){
    return fetch(apiUrl + endpoint, {
        method: 'get',
        headers: {
            'Authorization': localStorage.getItem('token'), 
            'content-type': 'application/json'
        }
    }).then((response) => {
        return response.json();
    })//.catch(function(){window.location.href = "errors/404.html";})
     ;
}

function post(endpoint, data){
    return fetch(apiUrl + endpoint,
    {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Authorization': localStorage.getItem('token'), 
            'content-type': 'application/json'
        }
    }).then((response) => {

        if(response.headers.get('Authorization')){
            localStorage.setItem('token', response.headers.get('Authorization'));
        }
        
        return response.json();
    })//.catch(function(){window.location.href = "errors/404.html";})
    ;
}

