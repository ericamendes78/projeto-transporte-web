const btnCadastrar = document.querySelector("#btn-cadastrar");
const btnVoltar = document.querySelector("#btnVoltar");


function CadastrarUsuario(){
  
    let data = { 
        nome : document.querySelector("#inputNome").value, 
        email : document.querySelector("#inputEmail").value,
        cpf: document.querySelector("#inputCpf").value,
        dataDeNascimento : document.querySelector("#inputDataDeNasciemnto").value,
        ddi : document.querySelector("#inputDDI").value,
        ddd : document.querySelector("#inputDDD").value,
        numeroTelefone : document.querySelector("#inputTelefone").value,
        cep : document.querySelector("#inputCep").value,
        logradouro : document.querySelector("#inputLogradouro").value,
        numeroLogradouro : document.querySelector("#inputNumero").value,
        complemento : document.querySelector("#inputComplemento").value,
        bairro : document.querySelector("#inputBairro").value,
        username : document.querySelector("#inputUsername").value,
        senha : document.querySelector("#inputSenha").value,
        tipoLembrete :  document.querySelector("#inputTipoLembrete").value
    };
    
    postIn(data);

    
}



function postIn(data) {
    fetch(apiUrl + "usuario/", {
      headers:{"Content-Type": "application/json"},
      method: 'POST',
      body: JSON.stringify(data)
    }).then(function(response) {
      if (response.ok){
        window.location.href = "";
      }else{
        window.location.href = "errors/404.html";
      }
    // return response.json();
    });
  }

btnCadastrar.onclick = CadastrarUsuario;
