﻿const divx = document.querySelector('#aviso');

var getOptions = {
        enableHighAccurancy: true, 
        timeout: 30000,
        maximumAge: 3000,
        desiredAccurancy: 3000
    };


function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.watchPosition(showPosition, showError,getOptions);
    } else {
        divx.innerHTML = "Geolocalização não é suportada nesse browser.";
    }
}

function showPosition(position) {
    lat = position.coords.latitude;
    lon = position.coords.longitude;
    latlon = new google.maps.LatLng(lat, lon)
    mapholder = document.getElementById('mapholder')
    mapholder.style.height = '350px';
    mapholder.style.width = '100%';

    var myOptions = {
        center: latlon,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        navigationControlOptions: {
            style: google.maps.NavigationControlStyle.SMALL
        }
    };
    var map = new google.maps.Map(document.getElementById("mapholder"), myOptions);
    var marker = new google.maps.Marker({
        position: latlon,
        map: map,
        title: "Você está Aqui!"
    });
}

function showError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            divx.innerHTML = "Usuário rejeitou a solicitação de Geolocalização."
            break;
        case error.POSITION_UNAVAILABLE:
          divx.innerHTML = "Localização indisponível."
            break;
        case error.TIMEOUT:
            divx.innerHTML = "O tempo da requisição expirou."
            break;
        case error.UNKNOWN_ERROR:
            divx.innerHTML = "Algum erro desconhecido aconteceu."
            break;
    }
}

 divx.onload = getLocation();