const div = document.querySelector('#funcao-transporte-metro');

function desenharLinha(linha){
    let divStatus = document.createElement("div");
    divStatus.id = linha.id;
    divStatus.alt = linha.line;
    divStatus.style.width = "300px";
    divStatus.style.height = "50px";
    divStatus.class = 'mb-4';

    let button = document.createElement("button");
    button.id = linha.id;
    button.alt = linha.line;
    button.style.width = "50px";
    button.style.height = "50px";    
    button.style.backgroundColor  = mudarCorLinha(linha.color);
    button.style.textAlign = "center";
    button.style.textJustify = "center";
    button.style.textTransform = "center";
    button.style.color = "white";
    button.style.borderRadius =" 30px 30px";
    button.disabled = "true";

    divStatus.appendChild(button);
    div.appendChild(button);
    div.appendChild(divStatus);

    button.innerHTML = `${linha.id}`;
    divStatus.innerHTML = `${linha.line} - ${linha.statusMetro} - ${linha.description}.`;

}

function parseData(dados){
    let linhas = dados.statusMetroPojo.listLineStatusPojo;
    let i = 0;
    for(i = 0; i < linhas.length; i++){
        desenharLinha(linhas[i]);
    }

}

function mudarCorLinha(corLinha){
    var cor;

    if(corLinha === "Azul"){
        cor = "blue";
    }else if(corLinha === "Verde"){
        cor = "green";
    }else if(corLinha === "Vermelha"){
        cor = "red";
    }else if(corLinha === "Lilás"){
        cor = "purple";
    }else if(corLinha === "Prata"){
        cor = "gray";
    }

    return cor;
}

function buscarStatusMetro(){
    get('status/metro').then(parseData);
}

div.onload = buscarStatusMetro();
